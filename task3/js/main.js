const imgArr = [
    'https://a-static.besthdwallpaper.com/thehunter-vyzov-dikoi-prirody-oboi-800x600-3640_17.jpg',
    'https://images.wallpaperscraft.ru/image/ozero_gory_derevia_122294_800x600.jpg',
    'https://assets.wam.ae/uploads/2020/01/large-1173661450844985080.jpg',
    'https://wallpapersmug.com/download/800x600/83db87/crater-lake-nature.jpg',
];

const dom = {
    img: document.querySelector('.slider__image'),
    imgContainer: document.querySelector('.slider__card'),
    btnPrev: document.querySelector('.slider_prev'),
    btnNext: document.querySelector('.slider_next'),
    counter: 1,
    step: 0,
};

dom.imgContainer.innerHTML = imgArr.map(url => `<img src="${url}" alt="slider-card" class="slider__image">`).join('');

dom.btnNext.addEventListener('click', () => {    

    dom.step += 800;
    dom.counter++;

    dom.imgContainer.style.left = `-${dom.step}px`;

   if(dom.counter > imgArr.length) {
        dom.counter = 1;
        dom.imgContainer.style.left = `${dom.step = 0}px`;
   }
    console.log(dom.counter);
    console.log(dom.step);
});

dom.btnPrev.addEventListener('click', () => {

    if(dom.counter === 1) {
        dom.imgContainer.style.left = `${dom.step = -800 * (imgArr.length - 1)}px`;
        dom.counter = imgArr.length;
    } else {
        dom.imgContainer.style.left = `${dom.step += 800}px`;
        dom.counter--;
    } 

   console.log(dom.counter);
   console.log(dom.step);
   
});



