const input = document.querySelector('.container__input');

const checkInput = () => {
    let regExp = /[\d]/ig;
    input.value = input.value.replace(regExp,'');
};

input.addEventListener('input', checkInput);
