//Task 1

const boardItem = Array.from(document.querySelectorAll('.board__item')),
      boardMessage = document.querySelector('.board__message'),
      clickAmount = document.querySelector('.click-amount');
      
let counterClick = 0,
    counter = 0;

const showBoardMessage = () => {
    boardMessage.style.display = 'flex';
    boardMessage.classList.add('animated', 'fadeInRight');
};

const getRandomColor = (min = 0, max = 255) => {
    return Math.floor(Math.random() * (max - min) + min);
};

const getRandomElement = () => {
    return boardItem.splice(Math.floor(Math.random() * boardItem.length), 1)[0];
};

const addElementOnBoard = () => {
    let randomItem = getRandomElement();
    randomItem.style.backgroundColor = `rgba(${getRandomColor()},${getRandomColor()},${getRandomColor()},1)`;
};

boardItem.forEach(item => item.addEventListener('click', () => {
    item.removeAttribute('style');
    boardItem.push(item);
    counterClick++;
   
    getClickAmount(counterClick);
}));

const getClickAmount = counterClick => {
    return counterClick;
};

const interval = setInterval(() => {
    addElementOnBoard();
    getClickAmount();

    clickAmount.innerHTML = `Click amount: ${counterClick}`;
    counter++;
    
    if(counter === 100 + counterClick) {
        showBoardMessage();
        clearInterval(interval);
    }
},50);


